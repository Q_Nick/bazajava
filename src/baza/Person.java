/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baza;

/**
 *
 * @author kamil
 */
public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private double weight;
    
    public Person(String firstName, String lastName, int age, double weight){
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;
        this.weight=weight;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String fistName) {
        this.firstName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    public void print(){
        System.out.println(this.getFirstName() + " " + this.getLastName() + " " + this.getAge() + " " + this.getWeight());
    }
    
}
