/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baza;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author kamil
 */
public class Main {

    private PersonList personList;

    public Main() {
        this.personList = new PersonList();
    }

    private void addPerson() {
        Scanner input = new Scanner(System.in);

        System.out.println("Imie");
        String name = input.next();

        System.out.println("Nazwisko");
        String lastName = input.next();

        System.out.println("Wiek");
        String sAge = input.next();
        int age = Integer.parseInt(sAge);

        System.out.println("Waga");
        String sWeight = input.next();
        double weight = Double.parseDouble(sWeight);

        Person person = new Person(name, lastName, age, weight);

        this.personList.addPerson(person);
    }
    
    private void savePersons(){
        try {
            personList.save();
        } catch (FileNotFoundException e) {
            e.getMessage();
        }
    }

    public static void menu() {
        System.out.println("Wybierz opcję:");
        System.out.println("1. Dodaj osobę");
        System.out.println("2. Wypisz osobę");
        System.out.println("3. Zapisz bazę");
        System.out.println("4. Odczytaj bazę");
        System.out.println("5. Wyjście");
        System.out.println("--------Zadania----------");
        System.out.println("6. Osoby 40+");
        System.out.println("7. Osoby 30-, 70-");
        System.out.println("8. Średni wiek");
        System.out.println("9. Maksymalna waga");
        System.out.println("10. Osoby cięższe niż średnia");
        System.out.println("11. Wyszukiwanie");
    }

    public void run() throws FileNotFoundException, IOException {
        Scanner input = new Scanner(System.in);

        boolean stop = false;

        do {
            menu();

            int option = input.nextInt();

            switch (option) {
                case 1:
                    addPerson();
                    break;
                case 2:
                    personList.print();
                    break;
                case 3:
                    savePersons();
                    break;
                case 4:
                    personList.load();
                    break;
                case 5:
                default:
                    stop = true;
                    break;
                case 6:
                    personList.olderThan40();
                    break;
                case 7:
                    personList.youngerThan30LighterThan70();
                    break;
                case 8:
                    personList.printMeanAge();
                    break;
                case 9:
                    personList.printMaxWeight();
                    break;
                case 10:
                    personList.printHeavierThanMean();
                    break;
                case 11:
                    personList.searchByName();
                    break;

            }
        } while (!stop);
    }

    public static void main(String[] args) {
        try {
            Main database = new Main();
            database.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
