/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baza;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Math.sqrt;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author kamil
 */
public class PersonList {

    private ArrayList<Person> personList;

    public PersonList() {
        this.personList = new ArrayList<>();
    }

    public void addPerson(Person person) {
        personList.add(person);
    }

    public Person getPerson(int index) {
        return personList.get(index);
    }

    public int size() {
        return personList.size();
    }

    public void removePerson(int index) {
        personList.remove(index);
    }

    public void save() throws FileNotFoundException {
        PrintWriter writer = new PrintWriter("src/data/base.txt");

        writer.println("My person base");

        for (int i = 0; i < size(); i++) {
            Person person = getPerson(i);

            writer.println(person.getFirstName() + " " + person.getLastName() + " " + person.getAge() + " " + person.getWeight());
        }
        writer.close();
    }

    public void load() throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader("src/data/base.txt"));

        reader.readLine();
        String line = reader.readLine();

        while (line != null) {
            StringTokenizer st = new StringTokenizer(line, " ");

            String firstName = st.nextToken();
            String lastName = st.nextToken();
            String sAge = st.nextToken();
            String sWeight = st.nextToken();

            int age = Integer.parseInt(sAge);
            double weight = Double.parseDouble(sWeight);

            Person person = new Person(firstName, lastName, age, weight);

            addPerson(person);

            line = reader.readLine();
        }

        reader.close();
    }

    public void print() {
        System.out.println("------------------Osoby z bazy danych----------------------");
        for (int i = 0; i < size(); i++) {
            Person person = getPerson(i);
            person.print();
        }
        System.out.println("-----------------------------------------------------------");
    }
    
    
    //---------------------Zadanie----------------------------
    
    public void olderThan40(){
        for(int i=0; i<personList.size(); i++){
            Person person=personList.get(i);
            
            if(person.getAge()>40){
                this.personList.get(i).print();
            }
        }
    }
    
    public void youngerThan30LighterThan70(){
        for(int i=0; i<personList.size(); i++){
            Person person=personList.get(i);
            
            if(person.getAge()<30 && person.getWeight()<=70){
                this.personList.get(i).print();
            }
        }
    }
    
    public double[] meanAge(){
        double totalAge=0;
        double mean, standardDeviation, counter=0;
        double[] result=new double[2];
        
        for(int i=0; i<this.personList.size(); i++){
            totalAge+=this.personList.get(i).getAge();
        }
        mean=totalAge/personList.size();
        
        for(int i=0; i<this.personList.size(); i++){
            counter+=sqrt(this.personList.get(i).getAge()+mean);
        }
        
        standardDeviation=counter/this.personList.size();
        
        result[0]=mean;
        result[1]=standardDeviation;
        
        return result;
    }
    
    public void printMeanAge(){
        double[] mean=meanAge();
        System.out.println("średni wiek to "+mean[0]+". Odchylenie standardowe wynosi "+mean[1]);
    }
    
    public double maxWeight(){
        double result=0;
        
        for(int i=0; i<this.personList.size(); i++){
            if(this.personList.get(i).getWeight()>result){
                result=this.personList.get(i).getWeight();
            }
        }
        
        return result;
    }
    
    public void printMaxWeight(){
        System.out.println("maksymalna waga to "+maxWeight());
    }
    
    public double meanWeight(){
        double meanWeight=0;
        
        for(int i=0; i<this.personList.size(); i++){
            meanWeight+=this.personList.get(i).getWeight();
        }
        
        return meanWeight/this.personList.size();
    }
    
    public void printMeanWeight(){
        System.out.println("średnia waga to "+meanWeight());
    }
    
    public void printHeavierThanMean(){
        double meanWeight=meanWeight();
        
        for(int i=0; i<this.personList.size(); i++){
            if(meanWeight<this.personList.get(i).getWeight()){
                this.personList.get(i).print();
            }
        }
    }
    
    public void searchByName(){
        Scanner input = new Scanner(System.in);
        
        System.out.println("Podaj imie");
        String name=input.next();
        
        System.out.println("Podaj nazwisko");
        String lastName=input.next();
        
        for(int i=0; i<this.personList.size(); i++){
            if(this.personList.get(i).getFirstName().equals(name) && this.personList.get(i).getLastName().equals(lastName)){
                this.personList.get(i).print();
            }
        }
    }
    
    //--------------------/Zadanie----------------------------

    public static void main(String[] args) {
            Person p1 = new Person("Jan", "Kowalski", 40, 80.6);
            Person p2 = new Person("Roman", "Kwiatkowski", 56, 78.9);
            Person p3 = new Person("Tadeusz", "Wolski", 34, 90.5);
            Person p4 = new Person("Ignacy", "Nowacki", 54, 87.7);

            PersonList personList = new PersonList();

            personList.addPerson(p1);
            personList.addPerson(p2);
            personList.addPerson(p3);
            personList.addPerson(p4);
            
            try{personList.save();}catch(Exception e){ e.getMessage();}

            personList.print();

            personList.removePerson(3);

            personList.print();
    }
}
